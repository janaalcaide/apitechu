package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String API_BASE_URL="/apitechu/v1";

    @GetMapping(API_BASE_URL + "/products")

    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }
    @GetMapping(API_BASE_URL + "/products/{id}")
    public ProductModel getProductByID(@PathVariable String id) {
        System.out.println ("getProductById");
        System.out.println ("is es " + id);
        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                result=product;
            }
        }
        return result ;
    }
    @PostMapping(API_BASE_URL+"/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println ("createProduct");
        System.out.println ("El id del nuevo producto es "+ newProduct.getId());
        System.out.println ("La descripción del nuevo producto es "+ newProduct.getDesc());
        System.out.println ("El precio del nuevo producto es "+ newProduct.getPrice());
        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }
    @PutMapping(API_BASE_URL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println ("updateProduct");
        System.out.println ("El id del  producto a actualizar es "+ id);
        System.out.println ("El id del  producto es "+ product.getId());
        System.out.println ("La descripción del producto a actualizar es "+ product.getDesc());
        System.out.println ("El precio del producto a actualizar es "+ product.getPrice());
        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());

            }
        }
        return product;
        }

    @DeleteMapping(API_BASE_URL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println ("deleteProduct");
        System.out.println ("El id del  producto a borrar es "+ id);
        ProductModel result=new ProductModel();
        boolean foundProduct = false;
        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto a borrar encontrado");
                foundProduct = true;
                result = productInList;

            }
        }
            if (foundProduct) {
                System.out.println ("Borrando producto");
                ApitechuApplication.productModels.remove(result);

            }
            return result;

    }
    @PatchMapping(API_BASE_URL + "/products/{id}")
    public ProductModel patchProduct (@RequestBody ProductModel product, @PathVariable String id){
        System.out.println ("PatchProduct");
        System.out.println ("El id del  producto a actualizar es "+ id);
        ProductModel result=new ProductModel();
        boolean patch_product = false;
        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto a actualizar encontrado");
                if (product.getDesc() != null){
                    productInList.setDesc(product.getDesc());
                    System.out.println ("La descripción del producto a actualizar es "+ product.getDesc());
                    System.out.println ("Descripción Actualizada");
                    patch_product = true;
                }
                if (product.getPrice() != 0.0f){
                    productInList.setPrice(product.getPrice());
                    System.out.println ("El precio del producto a actualizar es "+ product.getPrice());
                    System.out.println ("Precio Actualizado");
                    patch_product = true;
                }
                if (product.getId() != null){
                    System.out.println ("El id no puede actualizarse");
                }
                result = productInList;}}
                if (patch_product) {
                    System.out.println ("Se ha actualizado el producto");
                }
        return result;
            }
}
